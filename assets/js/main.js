document.addEventListener(
  "DOMContentLoaded",
  function() {
    function beginMotion(word, width, height, caller) {
      var enemy = document.getElementById(word);
      var player = document.getElementById("player");
      var red = document.getElementById("red");
      var green = document.getElementById("green");
      window.intervalID[word] = setInterval(function() {
        player.focus();
        width = width - 0.5;
        height = height - 0.5;
        enemy.style.marginLeft = width + "px";
        enemy.style.marginTop = height + "px";
        if (height < 0 || width < 0) {
          enemy.style.color = "red";
          clearInterval(window.intervalID[word]);
        } else if (player.value == word) {
          $.ajax({
            data: "id=" + window.id + "&word=" + word + "&caller=" + caller,
            url: "db/enemy_dead.php",
            method: "POST",
            success: function(ret) {}
          });
          enemy.style.textDecoration = "line-through";
          enemy.style.color = "green";
          green.innerText = parseInt(green.innerText) + 1;
          clearInterval(window.intervalID[word]);
          player.value = "";
        }
      }, 10);
    }
    function spawnEnemy(word) {
      var para = document.createElement("p");
      var node = document.createTextNode(word);
      para.appendChild(node);
      width = Math.floor(Math.random() * (window.width - 300 + 1)) + 300;
      height = Math.floor(Math.random() * (window.height - 300 + 1)) + 300;
      para.style.position = "absolute";
      para.style.marginLeft = width + "px";
      para.style.marginTop = height + "px";
      para.id = word;
      $.ajax({
        data:
          "id=" + window.id + "&word=" + word + "&x=" + width + "&y=" + height,
        url: "db/add_enemy.php",
        async: false,
        method: "POST",
        success: function(ret) {
          if (ret == "success") {
            document.body.appendChild(para);
            beginMotion(word, width, height, 1);
          }
        }
      });
    }

    function checkDeaths(caller) {
      $.ajax({
        data: "id=" + window.id + "&caller=" + caller,
        async: false,
        url: "db/ret_deaths.php",
        method: "POST",
        success: function(ret) {
          ret = JSON.parse(ret);
          if (ret.length != window.oppKills) {
            ret.forEach(function(item) {
              clearInterval(window.intervalID[item["word"]]);
              document.getElementById(item["word"]).style.color = "blue";
            });
            document.getElementById("blue").innerText = window.oppKills =
              ret.length;
          }
        }
      });
    }

    function p1_fetchEnemy() {
      fetch("https://random-word-api.herokuapp.com/word?")
        .then(word => word.json())
        .then(data => spawnEnemy(data[0]));
    }
    function p2_fetchEnemy() {
      $.ajax({
        data: "id=" + window.id,
        url: "db/get_enemy.php",
        async: false,
        method: "POST",
        success: function(ret) {
          ret = JSON.parse(ret);
          if (ret.length != window.enemyCount) {
            var para = document.createElement("p");
            var node = document.createTextNode(ret[window.enemyCount]["word"]);
            para.appendChild(node);
            para.style.position = "absolute";
            para.style.marginLeft = ret[window.enemyCount]["x"] + "px";
            para.style.marginTop = ret[window.enemyCount]["y"] + "px";
            para.id = ret[window.enemyCount]["word"];
            window.enemyCount++;
            document.body.appendChild(para);
            beginMotion(
              ret[window.enemyCount - 1]["word"],
              ret[window.enemyCount - 1]["x"],
              ret[window.enemyCount - 1]["y"],
              2
            );
          }
        }
      });
    }
    function spawnScoreBoard() {
      window.red = 0;
      window.green = 0;
      var width = window.screen.availWidth;
      var height = window.screen.availHeight;

      var red = document.createElement("p");
      var node = document.createTextNode("0");
      red.appendChild(node);
      red.id = "blue";
      red.style.position = "absolute";
      red.style.color = "blue";
      red.style.fontSize = "2em";
      red.style.marginTop = height - 200 + "px";
      red.style.marginLeft = width / 2 + 40 + "px";

      var green = document.createElement("p");
      node = document.createTextNode("0");
      green.appendChild(node);
      green.id = "green";
      green.style.position = "absolute";
      green.style.color = "green";
      green.style.fontSize = "2em";
      green.style.marginTop = height - 200 + "px";
      green.style.marginLeft = width / 2 - 50 + "px";

      var p = document.createElement("p");
      node = document.createTextNode("|");
      p.appendChild(node);
      p.style.position = "absolute";
      p.style.fontSize = "2em";
      p.style.marginTop = height - 200 + "px";
      p.style.marginLeft = width / 2 + "px";

      document.body.appendChild(red);
      document.body.appendChild(green);
      document.body.appendChild(p);
    }

    function spawnPlayer() {
      var inputField = document.createElement("input");
      inputField.id = "player";
      var width = window.screen.availWidth;
      var height = window.screen.availHeight;
      inputField.style.position = "absolute";
      inputField.style.textAlign = "center";
      inputField.style.width = width - 20 + "px";
      inputField.style.marginLeft = 0;
      inputField.style.marginTop = height - 150 + "px";
      inputField.autofocus = true;
      inputField.classList.add("field-control");
      document.body.appendChild(inputField);
    }

    function p1(e) {
      window.id = document.getElementById("session").value;
      e.previousElementSibling.remove();
      e.nextElementSibling.remove();
      e.remove();
      var node = document.createElement("h3");
      node.innerText = "Waiting for other player";
      node.style.position = "absolute";
      node.style.left = "48vw";
      node.style.top = "36vh";
      document.body.append(node);
      $.ajax({
        data: "id=" + window.id,
        url: "db/p1_ready.php",
        method: "POST",
        success: function(ret) {}
      });
      window.onbeforeunload = function() {
        $.ajax({
          data: "id=" + window.id,
          async: false,
          url: "db/del_session.php",
          async: true,
          method: "POST"
        });
        return null;
      };
      window.playerInt = setInterval(function() {
        $.ajax({
          data: "id=" + window.id,
          url: "db/both_ready.php",
          async: false,
          method: "POST",
          success: function(ret) {
            if (ret == "ready") {
              node.remove();
              clearInterval(window.playerInt);
              spawnPlayer();
              spawnScoreBoard();
              setInterval(p1_fetchEnemy, 3000);
              setInterval(function() {
                checkDeaths(2);
              }, 300);
            }
          }
        });
      }, 3000);
    }

    function p2(e) {
      window.id = document.getElementById("session").value;
      e.previousElementSibling.previousElementSibling.remove();
      e.previousElementSibling.remove();
      e.remove();
      var node = document.createElement("h3");
      node.innerText = "Waiting for other player";
      node.style.position = "absolute";
      node.style.left = "45vw";
      node.style.top = "36vh";
      document.body.append(node);
      $.ajax({
        data: "id=" + window.id,
        url: "db/p2_ready.php",
        method: "POST",
        success: function(ret) {}
      });
      window.onbeforeunload = function() {
        $.ajax({
          data: "id=" + window.id,
          async: false,
          url: "db/del_session.php",
          async: true,
          method: "POST"
        });
        return null;
      };
      window.playerInt = setInterval(function() {
        $.ajax({
          data: "id=" + window.id,
          url: "db/both_ready.php",
          async: false,
          method: "POST",
          success: function(ret) {
            if (ret == "ready") {
              node.remove();
              clearInterval(window.playerInt);
              spawnPlayer();
              spawnScoreBoard();
              setInterval(p2_fetchEnemy, 1000);
              setInterval(function() {
                checkDeaths(1);
              }, 300);
            }
          }
        });
      }, 1000);
    }
    window.enemyCount = 0;
    window.oppKills = 0;
    window.width = window.screen.availWidth;
    window.height = window.screen.availHeight;
    window.intervalID = [];
    document.getElementById("p1").addEventListener("click", function() {
      p1(this);
    });
    document.getElementById("p2").addEventListener("click", function() {
      p2(this);
    });
  },
  false
);
