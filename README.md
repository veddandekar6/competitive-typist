## **Competitive Typist**

A simple and fun game that puts your typing speed to test against other
opponents in a real time multiplayer experience.

Live Demo - <http://13.233.227.78/>

## **How to Play**

-   Enter Room ID (numeric) and enter as either Player 1 or Player 2.

-   Share Room ID with opponent and ask them to join as the other
    player.

-   Once both the players have joined the room, the game will begin with
    words floating across your screen.

-   Try to type the word before your opponent and before they touch the
    edge of the screen.

-   The score board on the bottom indicates the player's score in green
    and the opponents score in blue. Similarly, the words typed by the
    player are displayed in green and the ones typed by opponent are
    displayed in blue.

**Note:** If either of the player leaves the game mid-way, the
opponent's game will be frozen to conclude a winner based on the score.

## **Concept**

This simple game was developed mainly in JavaScript and PHP as a study
and a proof of concept. The main aim was to test the efficiency and
feasibility of implementing continuous polling of database on a system.
In this game, two calls are made to the database every second - one
every 300ms and another either every 3000ms or 1000ms, depending on
whether you are player 1 or 2.
